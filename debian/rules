#!/usr/bin/make -f
# debian/rules for unison package

include /usr/share/ocaml/ocamlvars.mk

VERSION := 2.53
UNISON_ABI := $(VERSION)

DOCS := src/TODO.txt unison-manual.txt NEWS.md

ifeq ($(OCAML_HAVE_OCAMLOPT),yes)
  NATIVE := true
else
  NATIVE := false
endif

SED_REPLACE := -e "s/@UNISON_PACKAGE@/unison-$(UNISON_ABI)/g"

SED_REPLACE_TEXT := -e "s/@UNISON_MAJ@/UNISON-$(UNISON_ABI)/g"
SED_REPLACE_TEXT += -e "s/@UNISON@/unison-$(UNISON_ABI)/g"

SED_REPLACE_GTK := -e "s/@UNISON_MAJ@/UNISON-$(UNISON_ABI)-GTK/g"
SED_REPLACE_GTK += -e "s/@UNISON@/unison-$(UNISON_ABI)-gtk/g"

%:
	dh $@ --with ocaml

override_dh_clean:
	dh_clean
	rm -f unison.1 debian/unison-$(UNISON_ABI).install debian/unison-$(UNISON_ABI)-gtk.install

override_dh_auto_configure:
	sed -e "s/@UnisonVersion@/$(VERSION)/g" -e "s/@UnisonABI@/$(UNISON_ABI)/g" debian/control.in > debian/control.new
	diff -u debian/control debian/control.new
	rm -f debian/control.new
	dh_auto_configure

override_dh_auto_clean:
	$(MAKE) clean
	rm -f src/ubase/depend
	rm -f unison-text unison-gtk
	rm -f unison-manual.txt unison.1
	rm -f debian/unison-gtk.png debian/unison-gtk.xpm

override_dh_auto_build: unison-text unison-gtk unison-manual.txt debian/unison-gtk.png debian/unison-gtk.xpm

override_dh_auto_install:
# Text version
	mkdir -p debian/tmp/usr/bin
	cp unison-text debian/tmp/usr/bin/unison-$(UNISON_ABI)
	mkdir -p debian/tmp/usr/share/man/man1
	sed $(SED_REPLACE) $(SED_REPLACE_TEXT) unison.1 > debian/tmp/usr/share/man/man1/unison-$(UNISON_ABI).1
	mkdir -p debian/tmp/usr/share/doc/unison-$(UNISON_ABI)
	cp $(DOCS) debian/tmp/usr/share/doc/unison-$(UNISON_ABI)
	sed $(SED_REPLACE) $(SED_REPLACE_TEXT) debian/unison.install.in > debian/unison-$(UNISON_ABI).install
# GTK version
	cp unison-gtk debian/tmp/usr/bin/unison-$(UNISON_ABI)-gtk
	sed $(SED_REPLACE) $(SED_REPLACE_GTK) unison.1 > debian/tmp/usr/share/man/man1/unison-$(UNISON_ABI)-gtk.1
	mkdir -p debian/tmp/usr/share/pixmaps
	cp debian/unison-gtk.xpm debian/tmp/usr/share/pixmaps/unison-$(UNISON_ABI)-gtk.xpm
	cp debian/unison-gtk.svg debian/tmp/usr/share/pixmaps/unison-$(UNISON_ABI)-gtk.svg
	mkdir -p debian/tmp/usr/share/applications
	sed $(SED_REPLACE) $(SED_REPLACE_GTK) debian/unison-gtk.desktop.in > debian/tmp/usr/share/applications/unison-$(UNISON_ABI)-gtk.desktop
	sed $(SED_REPLACE) $(SED_REPLACE_GTK) debian/unison-gtk.install.in > debian/unison-$(UNISON_ABI)-gtk.install

override_dh_auto_test:

unison-text:
	$(MAKE) clean
	$(MAKE) -C src UISTYLE=text NATIVE=$(NATIVE) CFLAGS=
	cp src/unison $@
	$(MAKE) manpage UISTYLE=text NATIVE=$(NATIVE) CFLAGS=
	cp man/unison.1 .

unison-gtk:
	$(MAKE) clean
	$(MAKE) -C src UISTYLE=gtk3 NATIVE=$(NATIVE) CFLAGS=
	cp src/unison $@

unison-manual.txt: unison-text
	./unison-text -doc all > $@

debian/unison-gtk.png: debian/unison-gtk.svg
	rsvg-convert -f png -w 32 -h 32 -o $@ $^

debian/unison-gtk.xpm: debian/unison-gtk.png
	convert $^ $@
